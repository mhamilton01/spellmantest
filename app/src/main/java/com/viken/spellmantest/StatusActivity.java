package com.viken.spellmantest;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class StatusActivity extends Activity {

    private String TAG = "StatusActivity";
    private int cntr = 0;

    // Status
    TextView HVEnable;
    TextView Interlock1Closed;
    TextView Interlock2Closed;
    TextView ECRModeActive;
    TextView PSFault;
    TextView LocalMode;
    TextView FilammentEnabled;
    TextView LargeFilament;
    TextView XraysEminent;
    TextView LargeFilamentConfirmation;
    TextView SmallFilamentConfirmation;
    TextView PowerSupplyReady;
    TextView InternalInterlockClosed;

    // Faults
    TextView filamentSelect;
    TextView overTemp;
    TextView overVoltage;
    TextView underVoltage;
    TextView overCurrent;
    TextView underCurrent;
    TextView overTempAnode;
    TextView overTempCathode;
    TextView inverterFaultAnode;
    TextView inverterFaultCathode;
    TextView filamentFeedback;
    TextView anodeArc;
    TextView cathodeArc;
    TextView cableConnectAnode;
    TextView cableConnectCathode;
    TextView acLineMonAnode;
    TextView aclineMonCathode;
    TextView dcRailMonAnode;
    TextView dcRailMonCathode;
    TextView lvpsNeg15stat;
    TextView lvpsPos15stat;
    TextView watchDog;
    TextView boardOverTemp;
    TextView overPower;
    TextView kvDiff;
    TextView maDiff;
    TextView inverterReady;

    // System voltages
    TextView temp;
    TextView kvAnode;
    TextView kvCathode;
    TextView acLineCathode;
    TextView dcRailCathode;
    TextView acLineAnode;
    TextView dcRailAnode;
    TextView lvpsPos15;
    TextView lvpsNeg15;

    private android.os.Handler timerHandler = new android.os.Handler();
    private Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {

            MainActivity.ps.Status();
            MainActivity.ps.Faults();
            MainActivity.ps.SysVoltages();

            SetTextColor(MainActivity.ps.status.HVEnable, HVEnable);
            SetTextColor(MainActivity.ps.status.Interlock1Closed, Interlock1Closed);
            SetTextColor(MainActivity.ps.status.Interlock2Closed, Interlock2Closed);
            SetTextColor(MainActivity.ps.status.ECRModeActive, ECRModeActive);
            SetTextColor(MainActivity.ps.status.PSFault, PSFault);
            SetTextColor(MainActivity.ps.status.LocalMode, LocalMode);
            SetTextColor(MainActivity.ps.status.FilammentEnabled, FilammentEnabled);
            SetTextColor(MainActivity.ps.status.LargeFilament, LargeFilament);
            SetTextColor(MainActivity.ps.status.XraysEminent, XraysEminent);
            SetTextColor(MainActivity.ps.status.LargeFilamentConfirmation, LargeFilamentConfirmation);
            SetTextColor(MainActivity.ps.status.SmallFilamentConfirmation, SmallFilamentConfirmation);
            SetTextColor(MainActivity.ps.status.PowerSupplyReady, PowerSupplyReady);
            SetTextColor(MainActivity.ps.status.InternalInterlockClosed, InternalInterlockClosed);

            SetTextColor(MainActivity.ps.faults.filamentSelect, filamentSelect);
            SetTextColor(MainActivity.ps.faults.overTemp, overTemp);
            SetTextColor(MainActivity.ps.faults.overVoltage, overVoltage);
            SetTextColor(MainActivity.ps.faults.underVoltage, underVoltage);
            SetTextColor(MainActivity.ps.faults.overCurrent, overCurrent);
            SetTextColor(MainActivity.ps.faults.underCurrent, underCurrent);
            SetTextColor(MainActivity.ps.faults.overTempAnode, overTempAnode);
            SetTextColor(MainActivity.ps.faults.overTempCathode, overTempCathode);
            SetTextColor(MainActivity.ps.faults.inverterFaultAnode, inverterFaultAnode);
            SetTextColor(MainActivity.ps.faults.inverterFaultCathode, inverterFaultCathode);
            SetTextColor(MainActivity.ps.faults.filamentFeedback, filamentFeedback);
            SetTextColor(MainActivity.ps.faults.anodeArc, anodeArc);
            SetTextColor(MainActivity.ps.faults.cathodeArc, cathodeArc);
            SetTextColor(MainActivity.ps.faults.cableConnectAnode, cableConnectAnode);
            SetTextColor(MainActivity.ps.faults.cableConnectCathode, cableConnectCathode);
            SetTextColor(MainActivity.ps.faults.acLineMonAnode, acLineMonAnode);
            SetTextColor(MainActivity.ps.faults.aclineMonCathode, aclineMonCathode);
            SetTextColor(MainActivity.ps.faults.dcRailMonAnode, dcRailMonAnode);
            SetTextColor(MainActivity.ps.faults.dcRailMonCathode, dcRailMonCathode);
            SetTextColor(MainActivity.ps.faults.lvpsNeg15, lvpsNeg15stat);
            SetTextColor(MainActivity.ps.faults.lvpsPos15, lvpsPos15stat);
            SetTextColor(MainActivity.ps.faults.watchDog, watchDog);
            SetTextColor(MainActivity.ps.faults.boardOverTemp, boardOverTemp);
            SetTextColor(MainActivity.ps.faults.overPower, overPower);
            SetTextColor(MainActivity.ps.faults.kvDiff, kvDiff);
            SetTextColor(MainActivity.ps.faults.maDiff, maDiff);
            SetTextColor(MainActivity.ps.faults.inverterReady, inverterReady);

            temp.setText(String.valueOf(MainActivity.ps.systemVoltages.temp));
            kvAnode.setText(String.valueOf(MainActivity.ps.systemVoltages.kvAnode));
            kvCathode.setText(String.valueOf(MainActivity.ps.systemVoltages.kvCathode));
            acLineCathode.setText(String.valueOf(MainActivity.ps.systemVoltages.acLineCathode));
            dcRailCathode.setText(String.valueOf(MainActivity.ps.systemVoltages.dcRailCathode));
            acLineAnode.setText(String.valueOf(MainActivity.ps.systemVoltages.acLineAnode));
            dcRailAnode.setText(String.valueOf(MainActivity.ps.systemVoltages.dcRailAnode));
            lvpsPos15.setText(String.valueOf(MainActivity.ps.systemVoltages.lvpsPos15));
            lvpsNeg15.setText(String.valueOf(MainActivity.ps.systemVoltages.lvpsNeg15));

            Log.d(TAG, "update status " + cntr++);
            timerHandler.postDelayed(this, 500);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        // Status
        HVEnable = (TextView) findViewById(R.id.HVEnable);
        Interlock1Closed = (TextView) findViewById(R.id.Interlock1Closed);
        Interlock2Closed = (TextView) findViewById(R.id.Interlock2Closed);
        ECRModeActive = (TextView) findViewById(R.id.ECRModeActive);
        PSFault = (TextView) findViewById(R.id.PSFault);
        LocalMode = (TextView) findViewById(R.id.LocalMode);
        FilammentEnabled = (TextView) findViewById(R.id.FilammentEnabled);
        LargeFilament = (TextView) findViewById(R.id.LargeFilament);
        XraysEminent = (TextView) findViewById(R.id.XraysEminent);
        LargeFilamentConfirmation = (TextView) findViewById(R.id.LargeFilamentConfirmation);
        SmallFilamentConfirmation = (TextView) findViewById(R.id.SmallFilamentConfirmation);
        PowerSupplyReady = (TextView) findViewById(R.id.PowerSupplyReady);
        InternalInterlockClosed = (TextView) findViewById(R.id.InternalInterlockClosed);

        // Faults
        filamentSelect = (TextView) findViewById(R.id.filamentSelect);
        overTemp = (TextView) findViewById(R.id.overTemp);
        overVoltage = (TextView) findViewById(R.id.overVoltage);
        underVoltage = (TextView) findViewById(R.id.underVoltage);
        overCurrent = (TextView) findViewById(R.id.overCurrent);
        underCurrent = (TextView) findViewById(R.id.underCurrent);
        overTempAnode = (TextView) findViewById(R.id.overTempAnode);
        overTempCathode = (TextView) findViewById(R.id.overTempCathode);
        inverterFaultAnode = (TextView) findViewById(R.id.inverterFaultAnode);
        inverterFaultCathode = (TextView) findViewById(R.id.inverterFaultCathode);
        filamentFeedback = (TextView) findViewById(R.id.filamentFeedback);
        anodeArc = (TextView) findViewById(R.id.anodeArc);
        cathodeArc = (TextView) findViewById(R.id.cathodeArc);
        cableConnectAnode = (TextView) findViewById(R.id.cableConnectAnode);
        cableConnectCathode = (TextView) findViewById(R.id.cableConnectCathode);
        acLineMonAnode = (TextView) findViewById(R.id.acLineMonAnode);
        aclineMonCathode = (TextView) findViewById(R.id.aclineMonCathode);
        dcRailMonAnode = (TextView) findViewById(R.id.dcRailMonAnode);
        dcRailMonCathode = (TextView) findViewById(R.id.dcRailMonCathode);
        lvpsNeg15stat = (TextView) findViewById(R.id.lvpsNeg15stat);
        lvpsPos15stat = (TextView) findViewById(R.id.lvpsPos15stat);
        watchDog = (TextView) findViewById(R.id.watchDog);
        boardOverTemp = (TextView) findViewById(R.id.boardOverTemp);
        overPower = (TextView) findViewById(R.id.overPower);
        kvDiff = (TextView) findViewById(R.id.kvDiff);
        maDiff = (TextView) findViewById(R.id.maDiff);
        inverterReady = (TextView) findViewById(R.id.inverterReady);

        // System voltages
        temp = (TextView) findViewById(R.id.temp);
        kvAnode = (TextView) findViewById(R.id.kvAnode);
        kvCathode = (TextView) findViewById(R.id.kvCathode);
        acLineCathode = (TextView) findViewById(R.id.acLineCathode);
        dcRailCathode = (TextView) findViewById(R.id.dcRailCathode);
        acLineAnode = (TextView) findViewById(R.id.acLineAnode);
        dcRailAnode = (TextView) findViewById(R.id.dcRailAnode);
        lvpsPos15 = (TextView) findViewById(R.id.lvpsPos15);
        lvpsNeg15 = (TextView) findViewById(R.id.lvpsNeg15);

        timerHandler.post(timerRunnable);
    }

    @Override
    protected void onDestroy() {
        timerHandler.removeCallbacks(timerRunnable);
        super.onDestroy();
    }

    private void SetTextColor(boolean field, TextView tv) {
        if (field)
            tv.setTextColor(Color.GREEN);
        else
            tv.setTextColor(Color.RED);

    }
}
