package com.viken.spellmantest;

public class CommsSerial extends Comms {

    static { System.loadLibrary("serialLib");}

    public native boolean serialOpen(String name);
    public native boolean serialClose();
    public native byte[] serialRead();
    public native boolean serialWrite(byte[] data);

    private String device;

    public CommsSerial(String dev) {
        device = dev;
    }

    @Override
    public boolean connect() {
        if (!serialOpen(device))
            return false;
        return true;
    }

    @Override
    public boolean disconnect() {
        if (!serialClose())
            return false;
        return true;
    }

    @Override
    public String read() {
        byte[] message = new byte[256];
        message = serialRead();
        return String.valueOf(message);
    }

    @Override
    public boolean write(String msg) {
        byte[] message = msg.getBytes();
        if (!serialWrite(message))
            return false;
        return true;
    }

    @Override
    public String status() {
        return null;
    }

    @Override
    public void delay() {
        //    try {
        //        Thread.sleep(delay);
        //    } catch (Exception e) { }
    }
}
