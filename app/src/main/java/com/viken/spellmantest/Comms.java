package com.viken.spellmantest;

public abstract class Comms {

    public String stx() {return Character.toString((char) 2);}
    public String etx() {return Character.toString((char) 3);}
    public String nul() {return Character.toString((char) 0);}

    public abstract boolean connect();

    public abstract String status();

    public abstract String read();

    public abstract boolean write(String msg);

    public abstract boolean disconnect();

    public abstract void delay();
}
