package com.viken.spellmantest;

import android.util.Log;

public class Spellman {

    private String tag = "Spellman";
    private String ip = "192.168.1.10";
    private int port = 50001;
    private Comms comms = null;

    private CommsType commsType;
    private AnalogMonitor analogMonitor;
    public Status status;
    public UserConfig userConfig;
    private Scaling scaling;
    private PowerLimits powerLimits;
    public Faults faults;
    public SystemVoltages systemVoltages;
    private int kVSetPoint;
    private int mASetPoint;
    private int filamentLimitSetPoint;
    private int filamentPreHeatSetPoint;
    private int totalHoursHVOn;
    private String dspSoftwareVersion;
    private String fpgaRevision;
    private int fpgaBuildNumber;
    private String modelNumber;
    private int kVMonitor;
    private int lvps;
    private boolean isEnabled = false;
    private float kvScale = 0f;
    private float maScale = 0f;

    public Spellman(CommsType type) {
        status = new Status();
        analogMonitor = new AnalogMonitor();
        userConfig = new UserConfig();
        scaling = new Scaling();
        powerLimits = new PowerLimits();
        faults = new Faults();
        systemVoltages = new SystemVoltages();
        commsType = type;
    }

    public boolean Connect() {
        switch (commsType) {
            case ETHERNET:
                comms = new CommsNet(ip, port);
                break;
            case SERIAL:
                comms = new CommsSerial("/dev/ttyUSB0");
                break;
            case USB:
                Log.e(tag, "USB device not implemented yet");
                break;
        }
        if (comms != null)
            if (comms.connect()) {
                try { Thread.sleep(1000); } catch (Exception e) {}
                Commands cmd = Commands.REQUESTSCALING;
                ProcessCmd(cmd);
                kvScale = 1f / ((float) scaling.voltage / 4096f);
                maScale = 1f / ((float) scaling.current / 4096f);
                return true;
            }
        return false;
    }

    public boolean Disconnect() {
        if (comms != null) {
            comms.disconnect();
            comms = null;
        }
        return true;
    }

    public void SetKV(int kv) {
        Commands cmd = Commands.PROGRAMKV;
        int val = (int) ((float) kv * kvScale);
        ProcessCmd(cmd, val);
    }

    public void SetMA(int ma) {
        Commands cmd = Commands.PROGRAMMA;
        int val = (int) ((float) ma * maScale);
        ProcessCmd(cmd, val);
    }

    public float GetKV() {
        Commands cmd = Commands.REQUESTANALOGMONITORREADBACKS;
        ProcessCmd(cmd);
        cmd = Commands.REQUESTKVSETPOINT;
        ProcessCmd(cmd);
        //return (float) analogMonitor.kV / kvScale;
        return (float) analogMonitor.kV * 0.06593f;
    }

    public float GetMA() {
        Commands cmd = Commands.REQUESTANALOGMONITORREADBACKS;
        ProcessCmd(cmd);
        cmd = Commands.REQUESTMASETPOINT;
        ProcessCmd(cmd);
        //return (float) analogMonitor.mA / maScale;
        return (float) analogMonitor.mA * 0.00879f;
    }

    public void Status() {
        Commands cmd = Commands.REQUESTSTATUS;
        ProcessCmd(cmd);
    }

    public void Faults() {
        Commands cmd = Commands.REQUESTFAULTS;
        ProcessCmd(cmd);
    }

    public String FaultStrings() {
        String faultText = "";
        if (faults.filamentSelect) {
            faultText += faults.filamentSelectString + "\n";
        }
        if (faults.overTemp) {
            faultText += faults.overTempString + "\n";
        }
        if (faults.overVoltage) {
            faultText += faults.overVoltageString + "\n";
        }
        if (faults.underVoltage) {
            faultText += faults.underVoltageString + "\n";
        }
        if (faults.overCurrent) {
            faultText += faults.overCurrentString + "\n";
        }
        if (faults.underCurrent) {
            faultText += faults.underCurrentString + "\n";
        }
        if (faults.overTempAnode) {
            faultText += faults.overTempAnodeString + "\n";
        }
        if (faults.overTempCathode) {
            faultText += faults.overTempCathodeString + "\n";
        }
        if (faults.inverterFaultAnode) {
            faultText += faults.inverterFaultAnodeString + "\n";
        }
        if (faults.inverterFaultCathode) {
            faultText += faults.inverterFaultCathodeString + "\n";
        }
        if (faults.filamentFeedback) {
            faultText += faults.filamentFeedbackString + "\n";
        }
        if (faults.anodeArc) {
            faultText += faults.anodeArcString + "\n";
        }
        if (faults.cathodeArc) {
            faultText += faults.cathodeArcString + "\n";
        }
        if (faults.cableConnectAnode) {
            faultText += faults.cableConnectAnodeString + "\n";
        }
        if (faults.cableConnectCathode) {
            faultText += faults.cableConnectCathodeString + "\n";
        }
        if (faults.acLineMonAnode) {
            faultText += faults.acLineMonAnodeString + "\n";
        }
        if (faults.aclineMonCathode) {
            faultText += faults.aclineMonCathodeString + "\n";
        }
        if (faults.dcRailMonAnode) {
            faultText += faults.dcRailMonAnodeString + "\n";
        }
        if (faults.dcRailMonCathode) {
            faultText += faults.dcRailMonCathodeString + "\n";
        }
        if (faults.lvpsNeg15) {
            faultText += faults.lvpsNeg15String + "\n";
        }
        if (faults.lvpsPos15) {
            faultText += faults.lvpsPos15String + "\n";
        }
        if (faults.watchDog) {
            faultText += faults.watchDogString + "\n";
        }
        if (faults.boardOverTemp) {
            faultText += faults.boardOverTempString + "\n";
        }
        if (faults.overPower) {
            faultText += faults.overPowerString + "\n";
        }
        if (faults.kvDiff) {
            faultText += faults.kvDiffString + "\n";
        }
        if (faults.maDiff) {
            faultText += faults.maDiffString + "\n";
        }
        if (faults.inverterReady) {
            faultText += faults.inverterReadyString + "\n";
        }
        return faultText;
    }

    public void Reset() {
        Commands cmd = Commands.RESETFAULTS;
        ProcessCmd(cmd);
    }

    public void SysVoltages() {
        Commands cmd = Commands.REQUESTSYSTEMVOLTAGES;
        ProcessCmd(cmd);
    }

    public void Enable(int kv, int ma) {
        // set filament limit
        Commands cmd = Commands.SETLARGESMALLFILAMENT;
        ProcessCmd(cmd, 1);
        int val = (int) (2400f / 225f * scaling.current); //max current ~10ma
        val = 4096*4/6; // 4amps, 6 is full scale (4096)
        cmd = Commands.PROGRAMFILAMENTLIMIT;
        ProcessCmd(cmd, val);
        cmd = Commands.PROGRAMHVON;
        ProcessCmd(cmd, 1);
        SetKV(kv);
        SetMA(ma);
        isEnabled = true;
    }

    public void Disable() {
        EnableHV(false);
        EnableFilament(false);
        isEnabled = false;
    }

    public void EnableHV(boolean state) {
        if (state) {
            Commands cmd = Commands.PROGRAMHVON;
            ProcessCmd(cmd, 1);
        } else {
            Commands cmd = Commands.PROGRAMHVON;
            ProcessCmd(cmd, 0);
        }
    }

    public void EnableFilament(boolean state) {
        if (state) {
            Commands cmd = Commands.FILAMENTCONTROL;
            ProcessCmd(cmd, 0);
        } else {
            Commands cmd = Commands.FILAMENTCONTROL;
            ProcessCmd(cmd, 0);
        }
    }

    public void SetPreHeatLimit(float value) {
        // not yet implemented
//        RequestAnalogMonitor();
//        analogMonitor.programPreHeat = (int) value; // need to figure out how the ramp rate is saved
//        SetAnalogParam();
    }

    public void SetKVRamp(float seconds) {
        RequestUserConfig();
        userConfig.rampTimeKV = (int) (seconds * 10f); // need to figure out how the ramp rate is saved
        SetUserConfig();
    }

    public void SetMARamp(float seconds) {
        RequestUserConfig();
        userConfig.rampTimeMA = (int) (seconds * 10f); // need to figure out how the ramp rate is saved
        SetUserConfig();
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void RequestAnalogMonitor() {
        Commands cmd = Commands.REQUESTANALOGMONITORREADBACKS;
        ProcessCmd(cmd);
    }

    public void SetAnalogParam() {
        //Commands cmd = Commands.REQUESTANALOGMONITORREADBACKS;
        //ProcessCmd(cmd);
    }

    private void RequestUserConfig() {
        Commands cmd = Commands.REQUESTUSERCONFIGURATION;
        ProcessCmd(cmd);
    }

    private void SetUserConfig() {
        Commands cmd = Commands.SETUSERCONFIGURATION;
        if (comms != null) {
            comms.write(BuildCmdUserConfig(cmd));
            comms.delay();
            if (!ValidateCmd(cmd, ParseResponse(comms.read())))
                Log.e(tag, "Failed to validate " + cmd.name);
        }
    }

    private void ProcessCmd(Commands cmd) {
        if (comms != null) {
            comms.write(BuildCmd(cmd));
            comms.delay();
            String resp = comms.read();
            String[] args = ParseResponse(resp);
            if (!ValidateCmd(cmd, args)) {
                Log.e(tag, "Failed to validate " + cmd.name);
                return;
            }
            SaveResponse(args);
        }
    }

    private void ProcessCmd(Commands cmd, int arg1) {
        if (comms != null) {
            comms.write(BuildCmd(cmd, arg1));
            comms.delay();
            if (!ValidateCmd(cmd, ParseResponse(comms.read())))
                Log.e(tag, "Failed to validate " + cmd.name);
        }
    }

    private String BuildCmd(Commands cmd) {
        String out = comms.stx();
        out += cmd.number + ",";
        out +=  comms.etx();
        return out;
    }

    private String BuildCmd(Commands cmd, int arg1) {
        String out = comms.stx();
        out += cmd.number + "," + arg1 + ",";
        out +=  comms.etx();
        return out;
    }

    private String BuildCmd(Commands cmd, int arg1, int arg2) {
        String out = comms.stx();
        out += cmd.number + "," + arg1 + "," + arg2 + ",";
        out +=  comms.etx();
        return out;
    }

    private String BuildCmdUserConfig(Commands cmd) {
        String out = comms.stx();
        out += /*cmd.number +*/ "09," +
                userConfig.arcQuenchScheme + "," +
                userConfig.overVoltagePercent + "," +
                userConfig.rampTimeKV + "," +
                userConfig.rampTimeMA + "," +
                userConfig.preWarnTime + "," +
                userConfig.arcCount + "," +
                "" + "," +
                userConfig.quenchTime + "," +
                "" + "," +
                userConfig.maxKV + "," +
                userConfig.maxMA + "," +
                userConfig.watchDogSeconds + "," +
                userConfig.filamentEnable + ",";
        out +=  comms.etx();
        return out;
    }

    /*
    Serial Interface protocol, XRV digital interface manual section 6.0

    <STX><CMD><,>ARG><,><CSUM><ETX>
    Where:
    <STX> = 1 ASCII 0x02 Start of Text character
    <CMD>= 2 ASCII characters representing the command ID
    <,> = 1 ASCII 0x2C character
    <ARG> = Command Argument
    <,> = 1 ASCII 0x2C character
    <CSUM> or <CS> = Checksum (see section 7.3 for details)
    <ETX> = 1 ASCII 0x03 End of Text character

     */
    private String[] ParseResponse(String resp) {
        String[] args = null;
        int idx = resp.indexOf(comms.stx())+1;
        if (idx >= 0) {
            String resp2 = resp.substring(idx);
            resp2.trim();
            if (resp2.endsWith(comms.etx()) | resp2.endsWith(comms.nul())) {
                resp2 = resp2.replace(comms.stx(), "");
                resp2 = resp2.replace(comms.etx(), "");
                args = resp2.split(",");
            }
        }
       return args;
    }

    private boolean ValidateCmd(Commands cmd, String[] args) {
        if (args != null) {
            if (cmd.number == Integer.valueOf(args[0]))
                return true;
        }
        Log.e("ValidateCmd", "expecting " + cmd.toString() + "(" + cmd.number + ")" + ", got " + Integer.valueOf(args[0]));
        return false;
    }

    private void SaveResponse(String[] args) {
        int responseId = Integer.valueOf(args[0]);
        int numArgs = numArgsResponse(responseId);
        if ((numArgs + 2) != args.length) {
            Log.e(tag, "Argument mismatch in Spellman:SaveResponse");
            Log.e(tag, "expected " + args.length + ", got " + String.valueOf(numArgs + 2));
            return;
        }
        switch(lookupResponse(responseId)) {
            case REQUESTKVSETPOINT:
                kVSetPoint = Integer.valueOf(args[1]);
                break;
            case REQUESTMASETPOINT:
                mASetPoint = Integer.valueOf(args[1]);
                break;
            case REQUESTSILAMENTLIMITSETPOINT:
                filamentLimitSetPoint = Integer.valueOf(args[1]);
                break;
            case REQUESTFILAMENTPREHEATSETPOINT:
                filamentPreHeatSetPoint = Integer.valueOf(args[1]);
                break;
            case REQUESTANALOGMONITORREADBACKS:
                analogMonitor.kV = Integer.valueOf(args[1]);
                analogMonitor.mA = Integer.valueOf(args[2]);
                analogMonitor.filament = Integer.valueOf(args[3]);
                analogMonitor.kVProgram = Integer.valueOf(args[4]);
                analogMonitor.mAProgram = Integer.valueOf(args[5]);
                analogMonitor.programLimit = Integer.valueOf(args[6]);
                analogMonitor.programPreHeat = Integer.valueOf(args[7]);
                analogMonitor.mAAnode = Integer.valueOf(args[8]);
                break;
            case REQUESTTOTALHOURSHVON:
                totalHoursHVOn = Integer.valueOf(args[1]);
                break;
            case REQUESTSTATUS:
                status.HVEnable = args[1].equals("1");
                status.Interlock1Closed = args[2].equals("1");
                status.Interlock2Closed = args[3].equals("1");
                status.ECRModeActive = args[4].equals("1");
                status.PSFault = args[5].equals("0");
                status.LocalMode = args[6].equals("1");
                status.FilammentEnabled = args[7].equals("1");
                status.LargeFilament = args[8].equals("1");
                status.XraysEminent = args[9].equals("1");
                status.LargeFilamentConfirmation = args[10].equals("1");
                status.SmallFilamentConfirmation = args[11].equals("1");
                status.PowerSupplyReady = args[16].equals("1");
                status.InternalInterlockClosed = args[17].equals("1");
                break;
            case REQUESTDSPSOFTWAREVERSION:
                dspSoftwareVersion = args[1];
                break;
            case REQUESTMODELNUMBER:
                modelNumber = args[1];
                break;
            case REQUESTUSERCONFIGURATION:
                userConfig.arcQuenchScheme = Integer.valueOf(args[1]);
                userConfig.overVoltagePercent = Integer.valueOf(args[2]);
                userConfig.rampTimeKV = Integer.valueOf(args[3]);
                userConfig.rampTimeMA = Integer.valueOf(args[4]);
                userConfig.preWarnTime = Integer.valueOf(args[5]);
                userConfig.arcCount = Integer.valueOf(args[6]);
                // 7 is reserved
                userConfig.quenchTime = Integer.valueOf(args[8]);
                // 9 is reserved
                userConfig.maxKV = Integer.valueOf(args[10]);
                userConfig.maxMA = Integer.valueOf(args[11]);
                userConfig.watchDogSeconds = Integer.valueOf(args[12]);
                userConfig.filamentEnable = Integer.valueOf(args[13]);
                break;
            case  REQUESTSCALING:
                scaling.voltage = Integer.valueOf(args[1]);
                scaling.current = Integer.valueOf(args[2]);
                scaling.bipolar = args[3].equals("1");
                break;
            case REQUESTPOWERLIMITS:
                powerLimits.largeLimit = Integer.valueOf(args[1]);
                powerLimits.smallLimit = Integer.valueOf(args[1]);
                powerLimits.filamentLargeLimit = Integer.valueOf(args[1]);
                powerLimits.filamentSmallLimit = Integer.valueOf(args[1]);
                powerLimits.preHeatLargeLimit = Integer.valueOf(args[1]);
                powerLimits.preHeatSmallLimit = Integer.valueOf(args[1]);
                break;
            case REQUESTFPGAREVISIONANDBUILD:
                fpgaRevision = args[1];
                fpgaBuildNumber = Integer.valueOf(args[2]);
                break;
            case REQUESTKVMONITOR:
                kVMonitor = Integer.valueOf(args[1]);
                break;
            case REQUESTLVPS:
                lvps = Integer.valueOf(args[1]);
                break;
            case REQUESTFAULTS:
                faults.filamentSelect = args[1].equals("0");
                faults.overTemp = args[2].equals("0");
                faults.overVoltage = args[3].equals("0");
                faults.underVoltage = args[4].equals("0");
                faults.overCurrent = args[5].equals("0");
                faults.underCurrent = args[6].equals("0");
                faults.overTempAnode = args[7].equals("0");
                faults.overTempCathode = args[8].equals("0");
                faults.inverterFaultAnode = args[9].equals("0");
                faults.inverterFaultCathode = args[10].equals("0");
                faults.filamentFeedback = args[11].equals("0");
                faults.anodeArc = args[12].equals("0");
                faults.cathodeArc = args[13].equals("0");
                faults.cableConnectAnode = args[14].equals("0");
                faults.cableConnectCathode = args[15].equals("0");
                faults.acLineMonAnode = args[16].equals("0");
                faults.aclineMonCathode = args[17].equals("0");
                faults.dcRailMonAnode = args[18].equals("0");
                faults.dcRailMonCathode = args[19].equals("0");
                faults.lvpsNeg15 = args[20].equals("0");
                faults.lvpsPos15 = args[21].equals("0");
                faults.watchDog = args[22].equals("0");
                faults.boardOverTemp = args[23].equals("0");
                faults.overPower = args[24].equals("0");
                faults.kvDiff = args[25].equals("0");
                faults.maDiff = args[26].equals("0");
                faults.inverterReady = args[27].equals("0");
                break;
            case REQUESTSYSTEMVOLTAGES:
                systemVoltages.temp = 0.05911815f * Integer.valueOf(args[1]);
                systemVoltages.kvAnode = 2400f * 0.2f * Integer.valueOf(args[3]) / 4095f;
                systemVoltages.kvCathode = 2400f * 0.2f * Integer.valueOf(args[4]) / 4095f;
                systemVoltages.acLineCathode = 0.088610f * Integer.valueOf(args[5]);
                systemVoltages.dcRailCathode = 0.11399241f * Integer.valueOf(args[6]);
                systemVoltages.acLineAnode = 0.088610f * Integer.valueOf(args[7]);
                systemVoltages.dcRailAnode = 0.11399241f * Integer.valueOf(args[8]);
                systemVoltages.lvpsPos15 = 0.00427407f * Integer.valueOf(args[9]);
                systemVoltages.lvpsNeg15 = 0.00576703f * Integer.valueOf(args[10]);
                break;
            default:
        }
    }

    private Response lookupResponse(int id) {
        for (Response r : Response.values()) {
            if (id == r.number) return r;
        }
        return null;
    }

    private Commands lookupCommand(int id) {
        for (Commands c : Commands.values()) {
            if (id == c.number) return c;
        }
        return null;
    }

    private int numArgsResponse(int id) {
        for (Response r : Response.values()) {
            if (id == r.number) return r.argLen;
        }
        return 0;
    }

    private int numArgsCommand(int id) {
        for (Commands c : Commands.values()) {
            if (id == c.number) return c.argLen;
        }
        return 0;
    }

    private enum Commands {
        SETUSERCONFIGURATION("User Configuration", 9, 13),
        PROGRAMKV("Program kV", 10, 1),
        PROGRAMMA("Program mA", 11, 1),
        PROGRAMFILAMENTLIMIT("Program Filament Limit", 12, 1),
        PROGRAMFILAMENTPREHEAT("Program Filament Pre-Heat", 13, 1),
        REQUESTKVSETPOINT("Request kV SetPoint", 14, 0),
        REQUESTMASETPOINT("Request mA SetPoint", 15, 0),
        REQUESTFILAMENTLIMITSETPOINT("Request Filament Limit SetPoint", 16, 0),
        REQUESTFILAMENTPREHEATSETPOINT("Request Filament Pre-Heat SetPoint", 17, 0),
        REQUESTANALOGMONITORREADBACKS("Request Analog Monitor Read backs", 19, 0),
        REQUESTHVONHOURSCOUNTER("Request HV On Hours Counter", 21, 0),
        REQUESTSTATUS("Request Status", 22, 0),
        REUQESTSOFTWAREVERSION("Request Software Version", 23, 0),
        REQUESTMODELNUMBER("Request Model Number", 26, 0),
        REQUESTUSERCONFIGURATION("Request User Configuration", 27, 0),
        REQUESTSCALING("Request Scaling", 28, 0),
        RESETHVONHOURSCOUNTER("Reset HV On Hours Counter", 30, 0),
        RESETFAULTS("Reset Faults", 31, 0),
        SETLARGESMALLFILAMENT("Set Large/Small Filament", 32, 1),
        REQUESTPOWERLIMITS("Request Power Limits", 38, 0),
        REQUESTFPGAREV("Request FPGA Rev", 43, 0),
        REQUESTKVMONITOR("Request KV Monitor", 60, 0),
        REQUESTLVPS("Request -15V LVPS", 65, 0),
        REQUESTFAULTS("Request Faults", 68, 0),
        REQUESTSYSTEMVOLTAGES("Request System Voltages", 69, 0),
        FILAMENTCONTROL("Filament Control", 70, 1),
        XRVCONTROLLERPRESET("XRV Controller Present", 71, 1),
        PROGRAMPOWERLIMITS("Program Power Limits", 97, 2),
        PROGRAMHVON("Program HV On", 98, 1),
        PROGRAMLOCALREMOTEMODE("Program Local/Remote Mode", 99, 1);

        private String name;
        private int number;
        private int argLen;

        Commands(String name, int number, int argLen) {
            this.name = name;
            this.number = number;
            this.argLen = argLen;
        }
    }

    private enum Response {
        USERCONFIGURATION("User Configuration", 9, 13),
        REQUESTKVSETPOINT("Request kV SetPoint", 14, 1),
        REQUESTMASETPOINT("Request mA SetPoint", 15, 1),
        REQUESTSILAMENTLIMITSETPOINT("Request Filament Limit SetPoint", 16,1),
        REQUESTFILAMENTPREHEATSETPOINT("Request Filament Pre-Heat SetPoint", 17, 1),
        REQUESTANALOGMONITORREADBACKS("Request Analog Monitor ReadBacks", 19, 8),
        REQUESTTOTALHOURSHVON("Request Total Hours HV On", 21, 1),
        REQUESTSTATUS("Request Status", 22, 17),
        REQUESTDSPSOFTWAREVERSION("Request DSP Software Version", 23, 1),
        REQUESTMODELNUMBER("Request Model Number", 26, 1),
        //REQUESTUSERCONFIGURATION("Request User Configuration", 27, 12),
        // changed the number args to 13, the spellman seems to provide 1 extra argument
        REQUESTUSERCONFIGURATION("Request User Configuration", 27, 13),
        REQUESTSCALING("Request Scaling", 28,3),
        REQUESTPOWERLIMITS("Request Power Limits", 38, 6),
        REQUESTFPGAREVISIONANDBUILD("Request FPGA Revision and Build", 43, 2),
        REQUESTKVMONITOR("Request kV Monitor", 60, 1),
        REQUESTLVPS("Request -15V LVPS", 65, 1),
        REQUESTFAULTS("Request Faults", 68, 27),
        REQUESTSYSTEMVOLTAGES("Request System Voltages", 69, 10);

        private String name;
        private int number;
        private int argLen;

        Response(String name, int number, int argLen) {
            this.name = name;
            this.number = number;
            this.argLen = argLen;
        }
    }

    public class AnalogMonitor {
        public int kV;
        public int mA;
        public int filament;
        public int kVProgram;
        public int mAProgram;
        public int programLimit;
        public int programPreHeat;
        public int mAAnode;
    }

    interface StatusIterator extends java.util.Iterator<Boolean> {}
    public class Status implements StatusIterator {
        public boolean HVEnable;
        public boolean Interlock1Closed;
        public boolean Interlock2Closed;
        public boolean ECRModeActive;
        public boolean PSFault;
        public boolean LocalMode;
        public boolean FilammentEnabled;
        public boolean LargeFilament;
        public boolean XraysEminent;
        public boolean LargeFilamentConfirmation;
        public boolean SmallFilamentConfirmation;
        public boolean PowerSupplyReady;
        public boolean InternalInterlockClosed;

        public boolean hasNext() {
            return true;
        }

        public Boolean next() {
            return true;
        }
    }

    public class UserConfig {
        public int arcQuenchScheme; // 1 roll back kv and ma, 0 do not rollback kv and ma
        public int overVoltagePercent;
        public int rampTimeKV; // tenths of second
        public int rampTimeMA; // tenths of second
        public int preWarnTime; // seconds
        public int arcCount;
        public int quenchTime; // milliseconds
        public int maxKV; // 0-4095
        public int maxMA; // 0-4095
        public int watchDogSeconds;
        public int filamentEnable;
    }

    private class Scaling {
        public int voltage;
        public int current;
        public boolean bipolar;
    }

    private class PowerLimits {
        public int largeLimit;
        public int smallLimit;
        public int filamentLargeLimit;
        public int filamentSmallLimit;
        public int preHeatLargeLimit;
        public int preHeatSmallLimit;
    }

    public class Faults {
        public boolean filamentSelect;
        public boolean overTemp;
        public boolean overVoltage;
        public boolean underVoltage;
        public boolean overCurrent;
        public boolean underCurrent;
        public boolean overTempAnode;
        public boolean overTempCathode;
        public boolean inverterFaultAnode;
        public boolean inverterFaultCathode;
        public boolean filamentFeedback;
        public boolean anodeArc;
        public boolean cathodeArc;
        public boolean cableConnectAnode;
        public boolean cableConnectCathode;
        public boolean acLineMonAnode;
        public boolean aclineMonCathode;
        public boolean dcRailMonAnode;
        public boolean dcRailMonCathode;
        public boolean lvpsNeg15;
        public boolean lvpsPos15;
        public boolean watchDog;
        public boolean boardOverTemp;
        public boolean overPower;
        public boolean kvDiff;
        public boolean maDiff;
        public boolean inverterReady;
        public String filamentSelectString = "Filament Select";
        public String overTempString = "Over Temp";
        public String overVoltageString = "Over Voltage";
        public String underVoltageString = "Under Voltage";
        public String overCurrentString = "Over Current";
        public String underCurrentString = "UnderCurrent";
        public String overTempAnodeString = "Over Temp Anode";
        public String overTempCathodeString = "Over Temp Cathode";
        public String inverterFaultAnodeString = "Inverter Fault Anode";
        public String inverterFaultCathodeString = "Inverter Fault Cathode";
        public String filamentFeedbackString = "Filament Feedback";
        public String anodeArcString = "Anode Arc";
        public String cathodeArcString = "Cathode Arc";
        public String cableConnectAnodeString = "Cable Connect Anode";
        public String cableConnectCathodeString = "Cable Connect Cathode";
        public String acLineMonAnodeString = "AC Line Monitor Anode";
        public String aclineMonCathodeString = "AC Line Monitor Cathode";
        public String dcRailMonAnodeString = "DC Rail Monitor Anode";
        public String dcRailMonCathodeString = "DC Rail Monitor Cathode";
        public String lvpsNeg15String = "LVPS Negative 15V";
        public String lvpsPos15String = "LVPS Positive 15V";
        public String watchDogString = "Watchdog";
        public String boardOverTempString = "Board Over Temp";
        public String overPowerString = "Over Power";
        public String kvDiffString = "kV Diff";
        public String maDiffString = "mA Diff";
        public String inverterReadyString = "Inverter Ready";
    }

    public class SystemVoltages {
        public float temp; // 0.05911815
        public float kvAnode; // (Unit Max * 20%) / 4095
        public float kvCathode;  // (Unit Max * 20%) / 4095
        public float acLineCathode; // 0.088610
        public float dcRailCathode; // 0.11399241
        public float acLineAnode; // 0.088610
        public float dcRailAnode; // 0.11399241
        public float lvpsPos15; // 0.00427407
        public float lvpsNeg15; // 0.00576703
    }

    public enum CommsType {
        ETHERNET,
        SERIAL,
        USB;
    }
}
