package com.viken.spellmantest;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

/**
 * Created by mhamilton on 8/16/16.
 */
public class WrapperSpinner extends Spinner {
    OnItemSelectedListener listener;

    public WrapperSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setSelection(int position) {
        super.setSelection(position);
        if (listener != null)
            listener.onItemSelected(null, null, position, 0);
    }

    public void setOnItemSelectedEvenIfUnchangedListener(OnItemSelectedListener listener) {
        this.listener = listener;
    }
}
