package com.viken.spellmantest;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class CommsNet extends Comms {

    private String ipAddress;
    private int port;
    private int delay = 50; // milleseconds
    private static final int SOCKET_TIMEOUT = 5000;
    private Socket socket;

    public CommsNet(String ip, int p) {
        ipAddress = ip;
        port = p;
    }

    @Override
    public boolean connect() {
        socket = new Socket();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //socket.bind(null);
                    InetSocketAddress addr = new InetSocketAddress(ipAddress, port);
                    socket.connect(addr, SOCKET_TIMEOUT);
                } catch (IOException e) {
                    e.printStackTrace();
                    //return false;
                }
           }
        }).start();
        return true;
    }

    @Override
    public boolean disconnect() {
        try {
            socket.close();
            socket = null;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public String read() {
        byte[] message = new byte[256];
        int nidx = 0;
        int sidx, eidx;
        try {
            DataInputStream is = new DataInputStream(socket.getInputStream());
            do {
                byte[] tmp = new byte[100];
                is.read(tmp);
                int tmpIdx = indexOf(tmp, nul().charAt(0));
                System.arraycopy(tmp, 0, message, nidx, tmpIdx);
                sidx = indexOf(message, stx().charAt(0));
                eidx = indexOf(message, etx().charAt(0));
                nidx = tmpIdx;
            } while ((sidx < 0) || (eidx < 0));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        //String resp = String.valueOf(message);
        String resp = new String(message, StandardCharsets.US_ASCII);
        return resp;
    }

    @Override
    public boolean write(String msg) {
        byte[] message = msg.getBytes();
        try {
            DataOutputStream os = new DataOutputStream(socket.getOutputStream());
            os.write(message);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public String status() {
        return null;
    }

    @Override
    public void delay() {
        try {
            Thread.sleep(delay);
        } catch (Exception e) { }
    }

    private int indexOf(byte[] b, char c) {
        for (int i = 0; i < b.length; i++) {
            if (b[i] == c)
                return i;
        }
        return -1;
    }
}
