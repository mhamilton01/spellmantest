package com.viken.spellmantest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity {

    public static Spellman ps = null;
    private TextView tv;
    private TextView voltageMon;
    private TextView currentMon;
    private TextView errorText;
    private ImageView hvImage;
    private ImageView filamentImage;
    private ImageView psReadImage;
    private ImageView xrayOnImage;
    private Activity myThis;

    private String TAG = "SpellmanTest";
    private int DELAYTIME = 1000;
    private int count = 0;
    private android.os.Handler updateHandler;
    private Runnable updateRunnable;

    private String host = "192.168.1.12";
    private int port = 15526;
    private final int SOCKET_TIMEOUT = 5000;
    public static final String ACTION_START    = "com.viken.utilslibrary.networkactions.VIKEN.SCAN.START";
    public static final String ACTION_STOP     = "com.viken.utilslibrary.networkactions.VIKEN.SCAN.STOP";
    private boolean conditioningRunning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myThis = this;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        hvImage = findViewById(R.id.hvEnableImage);
        filamentImage = findViewById(R.id.filamentEnableImage);
        psReadImage = findViewById(R.id.psReadyImage);
        xrayOnImage = findViewById(R.id.xrayOnImage);
        voltageMon = findViewById(R.id.voltageMon);
        currentMon = findViewById(R.id.currentMon);
        tv = findViewById(R.id.sample_text);
        tv.setText("text goes here");
        errorText = findViewById(R.id.errorText);

        Button connect = (Button) findViewById(R.id.connect);
        Button status = (Button) findViewById(R.id.status);
        final Button enable = (Button) findViewById(R.id.enable);
        Button disconnect = (Button) findViewById(R.id.disconnect);
        Button test = (Button) findViewById(R.id.test);
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ps.Test();
//                String txt = "current rate " + String.valueOf(ps.userConfig.currentRampRate/10f) + " seconds\n";
//                txt += "voltage rate " + String.valueOf(ps.userConfig.voltageRampRate/10f) + " seconds\n";
//                txt += "arcs " + String.valueOf(ps.userConfig.arcCount);
//                tv.setText(txt);
                ps.Disable();
                ps.SetKV(220);
                ps.SetMA(11);
                ps.SetPreHeatLimit(1.5f);
                ps.SetKVRamp(0.5f);
                ps.SetMARamp(1.0f);
                ps.EnableFilament(true);
            }
        });
        ToggleButton testSetup = (ToggleButton) findViewById(R.id.enaordis);
        testSetup.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    ps.EnableHV(true);
                } else {
                    // The toggle is disabled
                    ps.EnableHV(false);
                }
            }
        });
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ps = new Spellman(Spellman.CommsType.ETHERNET);
                ps.Connect();
                tv.setText("Connected");
            }
        });
        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ps == null) {
                    tv.setText("Not connected");
                    return;
                }
                ps.Reset();
                startActivity(new Intent(myThis, StatusActivity.class));
            }
        });
        final WrapperSpinner selectVolts = (WrapperSpinner) findViewById(R.id.selectVolts);
        selectVolts.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String str = (String) selectVolts.getItemAtPosition(position);
                int volts = Integer.valueOf(str);
                tv.setText("Voltage selected " + volts);
                ps.SetKV(volts);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final WrapperSpinner selectCurrent = (WrapperSpinner) findViewById(R.id.selectCurrent);
        selectCurrent.setOnItemSelectedEvenIfUnchangedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String str = (String) selectCurrent.getItemAtPosition(position);
                int current = Integer.valueOf(str);
                tv.setText("Current selected " + current);
                ps.SetMA(current);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        enable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ps.isEnabled()) {
                    ps.Disable();
                    enable.setText("Enable");
                } else {
                    int kv = Integer.valueOf(selectVolts.getSelectedItem().toString());
                    int ma = Integer.valueOf(selectCurrent.getSelectedItem().toString());
                    ps.Enable(kv, ma);
                    enable.setText("Disable");
                }
            }
        });
        Button startButton = findViewById(R.id.startButon);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    final Socket socket = new Socket();
                    //Log.d("CommandService", "Opening client socket");
                    socket.bind(null);
                    socket.connect(new InetSocketAddress(host, port), SOCKET_TIMEOUT);
                    //Log.d("CommandService", "Client socket - " + socket.isConnected());
                    ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                    oos.writeObject(ACTION_START);
                    oos.flush();
                    socket.close();
                } catch (IOException e) {
                    Log.e("startButton", "IOException " + e.getMessage());
                }
            }
        });
        Button stopButton = findViewById(R.id.stopButon);
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    final Socket socket = new Socket();
                    //Log.d("CommandService", "Opening client socket");
                    socket.bind(null);
                    socket.connect(new InetSocketAddress(host, port), SOCKET_TIMEOUT);
                    //Log.d("CommandService", "Client socket - " + socket.isConnected());
                    ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                    oos.writeObject(ACTION_STOP);
                    oos.flush();
                    socket.close();
                } catch (IOException e) {
                    Log.e("startButton", "IOException " + e.getMessage());
                }
            }
        });
        final Button conditioningButton = findViewById(R.id.conditioning);
        conditioningButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!conditioningRunning) {
                    conditionTube(false);
                    conditioningButton.setBackgroundColor(Color.RED);
                }
            }
        });
        conditioningButton.setBackgroundColor(Color.WHITE);

        final Button longconditioningButton = findViewById(R.id.longconditioning);
        longconditioningButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!conditioningRunning) {
                    conditionTube(true);
                    longconditioningButton.setBackgroundColor(Color.RED);
                }
            }
        });
        longconditioningButton.setBackgroundColor(Color.WHITE);

        disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ps.Disconnect();
            }
        });

        updateHandler = new android.os.Handler();
        updateRunnable = new Runnable() {
            @Override
            public void run() {
                updateThread();
                updateHandler.postDelayed(updateRunnable, DELAYTIME);
            }
        };
        updateHandler.postDelayed(updateRunnable, DELAYTIME);
        check();
    }

    private void updateThread() {
        if (ps != null) {
            float volts = ps.GetKV();
            float amps = ps.GetMA();
            Log.d("TAG", "Volts " + volts + ", amps " + amps);
            voltageMon.setText(String.valueOf(volts));
            currentMon.setText(String.valueOf(amps));
            //ps.Status();
            ps.Faults();
            errorText.setText(ps.FaultStrings());
            ps.Status();
            if (ps.status.FilammentEnabled)
                filamentImage.setImageResource(R.drawable.green);
            else
                filamentImage.setImageResource(R.drawable.red);
            if (ps.status.HVEnable)
                hvImage.setImageResource(R.drawable.green);
            else
                hvImage.setImageResource(R.drawable.red);
            if (ps.status.XraysEminent)
                xrayOnImage.setImageResource(R.drawable.green);
            else
                xrayOnImage.setImageResource(R.drawable.red);
            if (ps.status.PowerSupplyReady)
                psReadImage.setImageResource(R.drawable.green);
            else
                psReadImage.setImageResource(R.drawable.red);
        }
        //Log.d(TAG, "update thread run " + count++);
    }

    private android.os.Handler runnerHandler;
    private Runnable runner;
    private int seconds;
    private int kv = 0, ma = 0;
    private void conditionTube(final boolean longC) {
        conditioningRunning = true;
        seconds = 0;
        runnerHandler = new android.os.Handler();
        runner = new Runnable() {
            @Override
            public void run() {
                int msec = 5000;
                if (longC) {
                    kv = longKvWarmUp(seconds);
                    ma = longMaWarmUp(seconds);
                } else{
                    kv = shortKvWarmUp(seconds);
                    ma = shortMaWarmUp(seconds);
                }
                if (kv == 0) {
                    if (ps != null)
                        ps.Disable();
                    Button conditioningButton = findViewById(R.id.conditioning);
                    conditioningButton.setBackgroundColor(Color.WHITE);
                    conditioningRunning = false;
                    Log.d("conditionTube", "Disable");
                    tv.setText("conditioning done");
                } else {
                    if (ps != null)
                        ps.Enable(kv, ma);
                    String txt = "sec: " + seconds + " kv: " + kv + " ma: " + ma;
                    Log.d("conditionTube", txt);
                    tv.setText(txt);
                    runnerHandler.postDelayed(runner, msec);
                    seconds += msec/1000;
                }
            }
        };
        runnerHandler.post(runner);
    }

    private int shortKvWarmUp(int msec) {
        // staring voltage/current
        int volt = 100;
        if (msec >= 60)
            volt += 10;
        if (msec >= 120)
            volt += 10;
        if (msec >= 180)
            volt += 10;
        if (msec >= 240)
            volt += 10;
        if (msec >= 300)
            volt += 10;
        if (msec >= 360)
            volt += 10;
        if (msec >= 420)
            volt += 10;
        if (msec >= 480)
            volt += 10;
        if (msec >= 540)
            volt += 10;
        if (msec >= 600)
            volt += 10;
        if (msec >= 660)
            volt += 10;
        if (msec >= 720)
            // ~15 minutes full voltage
            volt += 10;
        if (msec >= 1200)
            // 20 minutes, done
            volt = 0;
        return volt;
    }

    private int shortMaWarmUp(int msec) {
        int cur = 4;
        if (msec >= 900)
            // ~15 minutes full power
            cur = 11;
        if (msec >= 1200)
            // 20 minutes, done.
            cur = 0;
        return cur;
    }

    private int longKvWarmUp(int msec) {
        // staring voltage/current
        int volt = 100;
        if (msec >= 180)
            volt += 10;
        if (msec >= 360)
            volt += 10;
        if (msec >= 540)
            volt += 10;
        if (msec >= 720)
            volt += 10;
        if (msec >= 900)
            volt += 10;
        if (msec >= 1080)
            volt += 10;
        if (msec >= 1260)
            volt += 10;
        if (msec >= 1440)
            volt += 10;
        if (msec >= 1620)
            volt += 10;
        if (msec >= 1800)
            volt += 10;
        if (msec >= 1980)
            volt += 10;
        if (msec >= 2160)
            // ~15 minutes full voltage
            volt += 10;
        if (msec >= 3600)
            // 20 minutes, done
            volt = 0;
        return volt;
    }

    private int longMaWarmUp(int msec) {
        int cur = 4;
        if (msec >= 2700)
            // ~50 minutes full power
            cur = 11;
        if (msec >= 3600)
            // 20 minutes, done.
            cur = 0;
        return cur;
    }

    private void check() {

        File sdcard = Environment.getExternalStorageDirectory();

        File file = new File(sdcard,"Android/challenge.txt");
        String line;
        String text = "";

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));

            while ((line = br.readLine()) != null) {
                text = line;
            }
            br.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            //You'll need to add proper error handling here
        }

        MessageDigest messageDigest = null;
        byte[] digest = new byte[0];

        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(text.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);

        while( md5Hex.length() < 32 ){
            md5Hex = "0" + md5Hex;
        }

        //Log.d("Encoded string: ", md5Hex);

        if (!md5Hex.equals("98a89cb39b77181066b73493ebe3392b")) {
            finish();
        }
    }

}
