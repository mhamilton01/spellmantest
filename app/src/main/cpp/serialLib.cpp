//
// Created by MarkHamilton on 6/19/2019.
//
#include <jni.h>
#include <stdio.h>
#include <string>
#include <fcntl.h>
#include <getopt.h>
#include <bits/ioctl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <zconf.h>
#include <errno.h>
#include <pty.h>
#include "android/log.h"

#define LOG_TAG "serialLib"
#define DLOG(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)

int fd = 0;

extern "C" JNIEXPORT jboolean JNICALL
Java_com_viken_spellmantest_CommsSerial_serialOpen(JNIEnv* env, jobject, jstring name) {
    jboolean status = true;
    const char* sname = env->GetStringUTFChars(name, nullptr);
    fd = open(sname, O_RDWR);
    env->ReleaseStringUTFChars(name, sname);
    if (fd < 0) {
        DLOG("failed to open serial %s errno 0x%x (%d) %s", sname, errno, errno, strerror(errno));
        status = false;
    }
    struct termios options;
    tcgetattr(fd, &options);
    cfsetispeed(&options, B115200);
    cfsetospeed(&options, B115200);
    tcsetattr(fd, TCSANOW, &options);
    return status;
}

extern "C" JNIEXPORT jboolean JNICALL
Java_com_viken_spellmantest_CommsSerial_serialClose(JNIEnv* env, jobject) {
    jboolean status = true;
    close(fd);
    fd = 0;
    return status;
}

extern "C" JNIEXPORT jbyteArray JNICALL
Java_com_viken_spellmantest_CommsSerial_serialRead(JNIEnv* env, jobject) {
    char buf[256];
    int n = read(fd, &buf, sizeof(buf));
    if (n <= 0) {
        DLOG("serial read failed");
        return 0;
    }
    jbyteArray ret = env->NewByteArray(n);
    env->SetByteArrayRegion(ret, 0, n, (jbyte*) buf);
    return ret;
}

extern "C" JNIEXPORT jboolean JNICALL
Java_com_viken_spellmantest_CommsSerial_serialWrite(JNIEnv* env, jobject, jbyteArray data) {
    jboolean status = true;
    jboolean isCopy;
    jbyte* buf = env->GetByteArrayElements(data, &isCopy);
    int n = write(fd, buf, sizeof(buf));
    if (n <= 0) {
        DLOG("serial read failed");
        status = false;
    }
    env->ReleaseByteArrayElements(data, buf, 0);
    return status;
}